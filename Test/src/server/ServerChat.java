/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.net.ServerSocket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTextArea;


/**
 *
 * @author 33766
 */
public class ServerChat extends Thread{
    public int nmClients;
    //private JTextArea textenv;
    //private JTextArea textrecu;
private List<Conversation> clientsConnectes = new ArrayList<>();
	
	public void run() {
		try {
			ServerSocket ss = new ServerSocket(1027);
			while(true){
				Socket s = ss.accept();
				++nmClients;
				Conversation c = new Conversation(s, nmClients);
				clientsConnectes.add(c);
				c.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void braoCast(String message, int[] numerClients){
		
		try {
			for(Conversation c:clientsConnectes){
				boolean trouve = false;
				for (int i = 0; i < numerClients.length; i++) {
					if (c.numeroClient == numerClients[i]) {
						trouve = true;
						break;
					}
				}
				if (trouve == true) {
					PrintWriter pw = new PrintWriter(c.socket.getOutputStream(),true);
					pw.println(message);
				}
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}
	
	public class Conversation extends Thread {
		
		protected Socket socket;
		protected int numeroClient;
		
		public Conversation(Socket socket,int num) {
			super();
			this.socket = socket;
			this.numeroClient = num;
		}
                
                public Conversation() {
			
		}

		public void run() {
			
			
			try {
				InputStream is = socket.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br =new BufferedReader(isr);
				
				OutputStream os =socket.getOutputStream();
				PrintWriter pw = new PrintWriter(os,true);
				
				String IP = socket.getRemoteSocketAddress().toString();
				
				System.out.println("Connextion client numéro "+ numeroClient + " ,IP "+ IP);
				pw.println("Bienvenue vous étes le client numéro "+ numeroClient);
				//pw.println("Deviner le nombre secret entre 0 et 1000");
				
				while(true){
					String req ;
					while((req = br.readLine()) != null){
						//System.out.println(IP + " à envoyé "+ req);
						String[] t = req.split("-");
						//message -1,3,5
						String message = t[0];
						String[] t2 = t[1].split(",");
						int[] numeroClients = new int[t2.length];
						for(int i=0; i < t2.length; i++){
							numeroClients[i] = Integer.parseInt(t2[i]);
						}
						braoCast(message,numeroClients);
					}	
					
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	}
	
	public static void main(String[] args) {
		new ServerChat().start();

	}
}
