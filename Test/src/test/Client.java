/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author 33766
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import javax.swing.JTextArea;

public class Client {
     private JTextArea textenv;
     private JTextArea textrecu;
     private PrintStream sortie;
     
     public Client(JTextArea textenv, JTextArea textrecu){
                Socket socket;
		BufferedReader in ;
	        PrintWriter out ;
		Scanner sc = new Scanner(System.in);//pour lire partir du clavier
		
		try {
			/*
			 * contenant les informations du serveur (port et adresse ip ou nom d'hote )
			 * 127.0.0.1 est l'adresse local de la machine
			 */
			socket = new Socket("127.0.0.1",1027);
			//flux pour envoyer
			out = new PrintWriter(socket.getOutputStream());
			//flux pour recevoir
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			/*
			 * // il faut créer deux processus (threads) parceque l'envoi et la reception se fait en méme 
			 * simultanémant cela nous permet d'envoyer et de recevoir des message en méme temps
			 */
			Thread envoyer = new Thread(new Runnable(){
				String msg;
				public void run(){
					//on considére que l'envoi et la reception des message 
					//snt infinis on le fait avec le boucle while
					while(true){
						msg = textenv.getText(); //sc.next();
						out.println(msg);
						out.flush();
					}
				}
				
			});
			envoyer.start();
			
			Thread recevoir = new Thread(new Runnable(){
				String msg;
				public void run(){
					//on considére que l'envoi et la reception des message 
					//snt infinis on le fait avec le boucle while
					while(true){
						try {
							msg = in.readLine();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						//System.out.println("<--- : "+msg);
                                                String sms = "<--- : "+msg;
                                                textrecu.setText(sms);
						
					}
				}
				
			});
			recevoir.start();
					
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     }
     
      public PrintStream recupererSortie()
    {
    	return sortie;
    	
    }

	/*public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Socket socket;
		final BufferedReader in ;
		final PrintWriter out ;
		final Scanner sc = new Scanner(System.in);//pour lire partir du clavier
		
		try {*/
			/*
			 * contenant les informations du serveur (port et adresse ip ou nom d'hote )
			 * 127.0.0.1 est l'adresse local de la machine
			 */
			/*socket = new Socket("127.0.0.1",1027);
			//flux pour envoyer
			out = new PrintWriter(socket.getOutputStream());
			//flux pour recevoir
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			*/
     /*
			 * // il faut créer deux processus (threads) parceque l'envoi et la reception se fait en méme 
			 * simultanémant cela nous permet d'envoyer et de recevoir des message en méme temps
			 */
			/*Thread envoyer = new Thread(new Runnable(){
				String msg;
				public void run(){
					//on considére que l'envoi et la reception des message 
					//snt infinis on le fait avec le boucle while
					while(true){
						msg = sc.next();
						out.println(msg);
						out.flush();
					}
				}
				
			});
			envoyer.start();
			
			Thread recevoir = new Thread(new Runnable(){
				String msg;
				public void run(){
					//on considére que l'envoi et la reception des message 
					//snt infinis on le fait avec le boucle while
					while(true){
						try {
							msg = in.readLine();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("<--- : "+msg);
						
					}
				}
				
			});
			recevoir.start();
					
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}*/

}
