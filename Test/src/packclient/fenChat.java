package packclient;


import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.net.* ;
import java.io.* ;


public class fenChat extends JFrame implements ActionListener{

		
		//private JLabel label1=new JLabel("Le java est de retour");
		private JPanel panneau=new JPanel();
		private JTextArea chat=new JTextArea(20,40);
		private JTextArea envoi=new JTextArea(10,40);
		private JButton Envoyer=new JButton("Envoyer");
		//private expedition exped;
		private PrintWriter sortie;
		private connexion connec;
		private String nom;
		private String serveur;
		
		
	

    	public fenChat(String n,int x,int y,int l,int L,String s) 
    	{
    		super(n);
    		setTitle("Client");
    		setBounds(x,y,l,L);
        	setResizable(true);
        	setVisible(true);
        	setAlwaysOnTop(true);
			setLayout(new FlowLayout ());
        	add("Center",chat);
        	add("Center",envoi);
        	add(Envoyer);
        	chat.setEditable(false);
        	envoi.requestFocusInWindow();
        	nom=n;
        	serveur=s;
        	
        	addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
       		 }});

        	
        	
        	Envoyer.addActionListener(this);
			
			
			connec=new connexion(chat,serveur);
			sortie=connec.recupererSortie();
			
			
			
			/*chat.addKeyListener(new KeyListener() 
			{
				public void keyTyped(KeyEvent e) 
					{
						if(e.getKeyChar()=='\n') 
						{
							envoyer();
							System.out.println("zob");
							
						}
		
				
					}

						
						
					

				public void keyPressed(KeyEvent e) {}

				public void keyReleased(KeyEvent e) {}
			});*/

			
			
			
        	show();
        	
			
    	}
    	
    	
    	
    	public void actionPerformed(ActionEvent e)              
		{ 
			
			String label = e.getActionCommand();
			
			if (label.equals("Envoyer")) 
        	{
        		envoyer();
        	
        		//System.out.println(connec.mote);
        	
        	
        	
        	}

		}
		
		
		public void envoyer()
		{
			String texte=envoi.getText();
        		sortie.println(texte);sortie.flush();
        		chat.setText(chat.getText()+nom+"  :\n"+texte+"\n");
        		envoi.setText(null);
        		envoi.requestFocusInWindow();
			
		}
	
			
		

    
    
}
